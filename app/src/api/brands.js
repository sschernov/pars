import request from '@/utils/axiosReq'


export function getBrands(data) {
  return request({
    url: '/api/core/brands/',
    data: data,
    method: 'get',
    isParams: true
  })
}

export function createBrand(data) {
  return request({
    url: '/api/core/brand/',
    data,
    method: 'post'
  })
}

export function updateBrand(data) {
  return request({
    url: '/api/core/brand/' + data.id +'/',
    data,
    method: 'patch'
  })
}

export function deleteBrand(data) {
  return request({
    url: '/api/core/brand/' + data,
    data,
    method: 'delete'
  })
}
