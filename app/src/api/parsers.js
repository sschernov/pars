import request from '@/utils/axiosReq'

export function getParsers(data) {
  return request({
    url: '/api/core/parsers/',
    data: data,
    method: 'get',
    isParams: true
  })
}

export function getParser(data) {
  return request({
    url: '/api/core/parsers/' + data + '/',
    method: 'get'
  })
}

export function runerTest(data) {
  return request({
    url: '/api/v1/testcode',
    method: 'post',
    data: data
  })
}

export function saveParser(data) {
  return request({
    url: '/api/v1/parser',
    data,
    method: 'post'
  })
}

export function deleteParser(data) {
  return request({
    url: '/api/core/parser/' + data + '/',
    data,
    method: 'delete'
  })
}
