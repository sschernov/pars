import request from '@/utils/axiosReq'


export function getResellers(data) {
  return request({
    url: '/api/core/resellers/',
    data: data,
    method: 'get',
    isParams: true
  })
}

export function getList(data) {
  return request({
    url: '/api/v1/getList',
    data: data,
    method: 'get',
    isParams: true
  })
}

export function createReseller(data) {
  return request({
    url: '/api/v1/reseller',
    data,
    method: 'post'
  })
}

export function updateReseller(data) {
  return request({
    url: '/api/v1/reseller',
    data,
    method: 'patch'
  })
}

export function deleteReseller(data) {
  return request({
    url: '/api/core/reseller/' + data,
    data,
    method: 'delete'
  })
}
