import request from '@/utils/axiosReq'

export function loginReq(data) {
  return request({
    url: 'api/token/',
    data,
    method: 'post',
    bfLoading: false,
    // isParams: true,
    isAlertErrorMsg: false
  })
}

export function getInfoReq() {
  return request({
    url: '/api/userinfo/',
    method: 'get'
  })
}

export function logoutReq() {
  return request({
    url: 'api-auth/logout',
    method: 'post'
  })
}
