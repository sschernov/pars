// import Vue from 'vue'
import VueI18n from 'vue-i18n'

import { getLanguage } from '@/utils/auth'
import enLocale from './en'
import elementEnLocale from 'element-plus/lib/locale/lang/en'
    // 'element-ui/lib/locale/lang/en'

// Vue.use(VueI18n)

const messages = {
  en: {
    ...enLocale,
    ...elementEnLocale
  }
}

export function getLocale() {
  const cookieLanguage = getLanguage()
  if (cookieLanguage) {
    document.documentElement.lang = cookieLanguage
    return cookieLanguage
  }

  const language = navigator.language.toLowerCase()
  const locales = Object.keys(messages)
  for (const locale of locales) {
    if (language.indexOf(locale) > -1) {
      document.documentElement.lang = locale
      return locale
    }
  }

  return 'en'
}

const i18n = new VueI18n({
  locale: getLocale(),
  messages
})

export default i18n
