import { createRouter, createWebHistory } from 'vue-router'
import Layout from '@/layout'

export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/Login.vue'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: () => import('@/views/dashboard/index'),
        meta: { title: 'Dashboard', icon: 'table' }
      }
    ]
  },
  {
    path: '/brands',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/brands/index'),
        name: 'Brands',
        meta: { title: 'Brands', icon: 'copyright' }
      }
    ]
  },
  {
    path: '/resellers',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/resellers/index'),
        name: 'Resellers',
        meta: { title: 'Resellers', icon: 'trademark' }
      }
    ]
  },
  {
    path: '/parsers',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/parsers/index'),
        name: 'Parsers',
        meta: { title: 'Parsers', icon: 'form' }
      }
    ]
  },
  {
    path: '/parser',
    component: Layout,
    children: [
      {
        path: '/parser/:id',
        component: () => import('@/views/parsers/edit'),
        name: 'Edit Parser',
        hidden: true,
        meta: { title: 'Edit Parsers', icon: 'form' }
      },
      {
        path: 'create',
        component: () => import('@/views/parsers/create'),
        name: 'Create Parser',
        hidden: true,
        meta: { title: 'Create Parser', icon: 'form' }
      }
    ]
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = []

const router = createRouter({
  history: createWebHistory(),
  scrollBehavior: () => ({ top: 0 }),
  routes: constantRoutes
})

export default router
