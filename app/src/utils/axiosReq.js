import store from '@/store'
import axios from 'axios'
import { ElLoading, ElMessage, ElMessageBox } from 'element-plus'
import { getToken, setToken } from '@/utils/auth'
import { getCurrentInstance } from 'vue'
let requestData
let loadingE
const HTTP_AUTHORIZATION = 'Authorization'

const service = axios.create({
  // baseURL: process.env.VUE_APP_BASE_URL,
  // timeout: 30000 // 超时时间
})

service.interceptors.request.use(

  (request) => {
    // debugger
    if (getToken()) {
      request.headers[HTTP_AUTHORIZATION] = 'Bearer ' + getToken()
    }
    // console.log('request', request)
    requestData = request
    // if (request.bfLoading) {
    //   loadingE = ElLoading.service({
    //     lock: true,
    //     text: '数据载入中',
    //     spinner: 'el-icon-loading',
    //     background: 'rgba(0, 0, 0, 0.1)'
    //   })
    // }
    /*
     *params
     * */
    if (request.isParams) {
      request.params = request.data
      request.data = {}
    }
    return request
  },
  (err) => {
    Promise.reject(err)
  }
)

service.interceptors.response.use(
  (res) => {
    // debugger
    console.log('ORIGINAL RESPONSE : ', res)

    // if (requestData.afHLoading && loadingE) {
    //   loadingE.close()
    // }
    //
    // if (requestData.isDownLoadFile) {
    //   return res.data
    // }

    const { data, status } = res
    switch (status) {
      case 200:
        return data
      case 201:
      case 204:
        location.reload()
        break
      default:
        debugger
        ElMessage({
          message: 'OOOPSSS. Something  wrong',
          type: 'error',
          duration: 10 * 1000
        })
        return Promise.reject(data)
    }
  },
  (err) => {
    // debugger
    if (loadingE) loadingE.close()
    if (err && err.response && err.response.code) {
      if (err.response.code === 401) {
        ElMessage({
          message: err,
          type: 'error',
          duration: 2 * 1000
        })
        store.dispatch('user/resetToken').then(() => {
          let { proxy } = getCurrentInstance()
          proxy.$router.push('/login')
        })
      } else if (err.response.code === 403) {
        ElMessageBox.confirm('Do not Permission', {
          confirmButtonText: 'Ok',
          cancelButtonText: 'Cancel',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      } else {
        ElMessage({
          message: err,
          type: 'error',
          duration: 2 * 1000
        })
      }
    } else {
      ElMessage({
        message: err,
        type: 'error',
        duration: 2 * 1000
      })
    }
    return Promise.reject(err)
  }
)

export default function khReqMethod({
  url,
  data,
  method,
  isParams,
  bfLoading,
  afHLoading,
  isUploadFile,
  isDownLoadFile,
  baseURL,
  timeout,
  isAlertErrorMsg
}) {
  return service({
    url: url,
    method: method ?? 'post',
    data: data ?? {},
    isParams: isParams ?? false,
    bfLoading: bfLoading ?? true,
    afHLoading: afHLoading ?? true,
    isUploadFile: isUploadFile ?? false,
    isDownLoadFile: isDownLoadFile ?? false,
    isAlertErrorMsg: isAlertErrorMsg ?? true,
    baseURL: baseURL ?? import.meta.env.VITE_APP_BASE_URL, // 设置基本基础url
    timeout: timeout ?? 15000 // 配置默认超时时间
  })
}
