from ninja import Schema, ModelSchema
from typing import List, Optional, Dict, Union
from django.apps import apps
# from backend.coredb.models import brand



class BrandModelSchema(ModelSchema):
    class Config:
        model = apps.get_model('coredb', 'brand')
        model_fields = ['id', 'name', 'hb2uri']

class ResellerModelSchema(ModelSchema):
    class Config:
        model = apps.get_model('coredb', 'reseller')
        model_fields = ['id', 'name', 'hb2uri']

class ParserModelSchema(ModelSchema):
    parserBrands: Optional[BrandModelSchema] = None
    parserReseller: Optional[ResellerModelSchema] = None

    class Config:
        model = apps.get_model('coredb', 'parser')
        model_exclude = ['created_at', 'updated_at']

class Resseler(Schema):
    id: int
    name: str
    hb2uri: str


class Brand(Schema):
    id: int
    name: str
    hb2uri: str
    reseller: Optional[Resseler] = None

class ProductData(Schema):
    price1: Optional[str] = None
    price2: Optional[str] = None
    stc1: Optional[str] = None
    stc2: Optional[str] = None
    url: Optional[str] = None

class ResponseData(Schema):
    brand: str
    products: Optional[Dict[str, ProductData]] = None

class ListData(Schema):
    id: int
    name: str
    hb2uri: str

class ResponseParser(Schema):
    code: str
    data: Optional[ParserModelSchema] = None
    error: Optional[str] = None

class Respons(Schema):
    code: str
    data: Optional[ResponseData] = None
    error: Optional[str] = None

class ResponseList(Schema):
    code: str
    data: Optional[List[ListData]] = None
    error: Optional[str] = None

class PartNumberReq(Schema):
    number: str

class BrandReq(Schema):
    name: str

class CodePaylodReq(Schema):
    language: str
    code: str
    arg1: str
    arg2: str

class CodePaylodResult(Schema):
    result: str = None
    error: str = None

class CodePayloadResponse(Schema):
    code: str
    data: CodePaylodResult = None
    error: str = None

class ResellerPayload(Schema):
    id: str = None
    name: str
    hb2uri: str
    brands: List[Dict[str, int]] = None

class Req1(Schema):
    brand: BrandReq
    partnums: List[PartNumberReq]






