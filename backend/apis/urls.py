import subprocess
import json
from ninja import Router
from typing import List
from django.apps import apps
from django.shortcuts import get_object_or_404
from . import schemas
# from coredb.models import Brand
from django.db import IntegrityError

router_api = Router()

@router_api.post("/testcode", response=schemas.CodePayloadResponse)
def test_code(request, payload: schemas.CodePaylodReq):
    data = payload.dict()
    code = data['code'][3:]
    code_type = data['language']
    # partnums = data['arg2']
    command = [code_type, '-r', code, f"{data['arg1']}", f"{data['arg2']}"]

    proc = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    print(stdout)
    print(stderr)
    if stderr or not stdout.decode('utf-8'):
        schemas.Respons.code = '200'
        schemas.Respons.data = {"error": stderr.decode('utf-8')}
        schemas.Respons.error = ''
        return schemas.Respons

    res1 = stdout.decode('utf-8')
    schemas.Respons.code = '200'
    schemas.Respons.data = {"result": res1}
    schemas.Respons.error = ''
    return schemas.Respons

@router_api.get("/getPrice", response=schemas.Respons)
def getPrice(request, data: str):
    # brand: str, partnums: List[str]
    # "{'brand':'zebra','partnums':['03200BK11045-EA','SAC-ET5X-4PPK1-01']}"

    data = data.replace("'", '"')[1:-1]
    json_data = json.loads(data)
    brand=json_data['brand']
    partnums=json_data['partnums']

    if not partnums or not brand:
        schemas.Respons.code = '400'
        schemas.Respons.data = None
        schemas.Respons.error = "Don't receive correct params brand or partnums."
        return schemas.Respons

    brand_model = apps.get_model('coredb', 'brand')
    brands = brand_model.objects.get(name=brand)
    if not brands:
        schemas.Respons.code = '404'
        schemas.Respons.data = None
        schemas.Respons.error = 'not found brand'
        return schemas.Respons

    parser_model = apps.get_model('coredb', 'parser')
    if not brands.reseller:
        parsers = parser_model.objects.get(parser_brands=brands.id)
        msg = ' for brand'
    else:
        parsers = parser_model.objects.get(parser_reseller=brands.reseller.id)
        msg = ' for reseller'

    if not parsers:
        schemas.Respons.code = '404'
        schemas.Respons.data = None
        schemas.Respons.error = f'Not found parsers {msg}'
        return schemas.Respons

    code = parsers.code[3:]
    code_type = parsers.language
    command = [code_type, '-r', code, f"{brands.name}",f"{','.join(partnums)}"]
    proc = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE,  stderr=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    print(f'result: {stdout} error: {stderr}')
    print(f"check: {not stdout.decode('utf-8')}")
    if stderr or not stdout.decode('utf-8'):
        schemas.Respons.code = '500'
        schemas.Respons.data = None
        schemas.Respons.error = f"Error with run script : {stderr.decode('utf-8')}"
        return schemas.Respons
    res1 = json.loads(stdout.decode('utf-8'))
    schemas.Respons.code = '200'
    schemas.Respons.data = res1
    schemas.Respons.error = parsers.name + msg
    return schemas.Respons


@router_api.get("/getList", response=schemas.ResponseList)
def getList(request, data: str):
    if not data:
        schemas.Respons.code = '400'
        schemas.Respons.data = None
        schemas.Respons.error = "Don't receive correct received."
        return schemas.Respons

    model = apps.get_model('coredb', data)
    if not model:
        schemas.Respons.code = '400'
        schemas.Respons.data = None
        schemas.Respons.error = f"Can't found model {data}."
        return schemas.Respons

    result = model.objects.all()
    print(result)

    schemas.ResponseList.code = '200'
    schemas.ResponseList.data = result
    schemas.ResponseList.error = None
    return schemas.ResponseList


@router_api.post("/reseller", response=schemas.ResponseList )
def create_reseller (request, payload: schemas.ResellerPayload):
    data = payload.dict()
    brands = apps.get_model('coredb', 'brand')
    resellers = apps.get_model('coredb', 'reseller')

    try:
        reseller = resellers.objects.create(name=data['name'], hb2uri=data['hb2uri'])
    except IntegrityError :
        schemas.ResponseList.code = '400'
        schemas.ResponseList.data = None
        schemas.ResponseList.error = 'Duplicate data'
        return schemas.ResponseList

    print(reseller.id)
    if (data['brands']):
        for row in data['brands']:
            try:
                brand = brands.objects.filter(id=row['id']).update(reseller=reseller.id)
            except brands.DoesNotExist:
                continue

    schemas.ResponseList.code = '201'
    schemas.ResponseList.data = None
    schemas.ResponseList.error = None
    return schemas.ResponseList

@router_api.patch("/reseller", response=schemas.ResponseList )
def create_reseller (request, payload: schemas.ResellerPayload):
    data = payload.dict()
    brands = apps.get_model('coredb', 'brand')
    resellers = apps.get_model('coredb', 'reseller')

    try:
        reseller = resellers.objects.get(id=data['id'])
    except reseller.DoesNotExist :
        schemas.ResponseList.code = '400'
        schemas.ResponseList.data = None
        schemas.ResponseList.error = 'Not found data'
        return schemas.ResponseList

    reseller.name = data['name']
    reseller.hb2uri = data['hb2uri']
    reseller.save()
    brands1 = brands.objects.filter(reseller=data['id']).update(reseller=None)
    if (data['brands']):
        for row in data['brands']:
            try:
                brand = brands.objects.filter(id=row['id']).update(reseller=reseller.id)
            except brands.DoesNotExist:
                continue


    schemas.ResponseList.code = '201'
    schemas.ResponseList.data = None
    schemas.ResponseList.error = None
    return schemas.ResponseList

@router_api.post("/parser", response=schemas.ResponseParser )
def create_parser (request, payload: schemas.ParserModelSchema):
    data = payload.dict()
    print(data)
    id = data['id']
    parserBrand = data['parserBrands']
    parserReseller = data['parserReseller']
    del data['id']
    del data['parserBrands']
    del data['parserReseller']
    print(data)
    parser = apps.get_model('coredb', 'parser')
    try:
        # parser = parser.objects.create(**data) if id == 0 else parser.objects.filter(id=id).update(**data)
        if id==0:
            result = parser.objects.create(**data)
        else:
            parser.objects.filter(id=id).update(**data)
            result = parser.objects.get(pk=id)
    except IntegrityError :
        schemas.ResponseList.code = '400'
        schemas.ResponseList.data = None
        schemas.ResponseList.error = 'Duplicate data'
        return schemas.ResponseList

    brand = apps.get_model('coredb', 'brand')
    reseller = apps.get_model('coredb', 'reseller')
    print(result.id)
    brand.objects.filter(parser=result.id).update(parser=None)
    reseller.objects.filter(parser=result.id).update(parser=None)

    if parserBrand:
        print('brand')
        brand.objects.filter(id=parserBrand['id']).update(parser=result.id)
    elif parserReseller:
        print('reseller')
        reseller.objects.filter(id=parserReseller['id']).update(parser=result.id)



    schemas.ResponseList.code = '200'
    schemas.ResponseList.data = result
    schemas.ResponseList.error = None
    return schemas.ResponseList

@router_api.get("/add")
def add(request, a: int, b: int):
    return {"result": a + b}

