from rest_framework_json_api.pagination import JsonApiPageNumberPagination

class MyPagePagination(JsonApiPageNumberPagination):
    page_query_param = 'page'
    page_size_query_param = 'size'
    page_size = 10
    max_page_size = 1000