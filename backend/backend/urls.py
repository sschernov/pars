import json

from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include

from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets, status

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from ninja import NinjaAPI
from apis.urls import router_api as api_routes


api = NinjaAPI()


schema_view = get_schema_view(
    openapi.Info(
        title="Parsers API",
        default_version='v1',
        description="Test description",
        contact=openapi.Contact(email="admin@example.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(IsAuthenticated,),
)




# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']
        # fields = '__all__'


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)


class UserInfoSerializer(serializers.ListSerializer):
    class Meta:
        fields = ['user', 'roles', 'name', 'avatar', 'introduction', 'email']


class UserInfoView(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserInfoSerializer

    def list(self, request):
        data = {
            'user': {
                'roles': ['admin'] if request.user.is_superuser else ['editor'],
                'name': request.user.first_name + ' ' + request.user.last_name,
                'avatar': 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
                'introduction': 'introduction',
                'email': request.user.email
            }
        }

        return Response(data)


@method_decorator(
    name='post',
    decorator=swagger_auto_schema(
        responses={
            status.HTTP_200_OK: openapi.Schema(
                title='TokenPair',
                type='object',
                properties={
                    'refresh': openapi.Schema(title='Refresh token', type='string'),
                    'access': openapi.Schema(title='Access token', type='string'),
                },
                required=['access', 'refresh']
            )
        },
    )
)

class MyTokenView(TokenObtainPairView):
    pass


@method_decorator(
    name='post',
    decorator=swagger_auto_schema(
        responses={
            status.HTTP_200_OK: openapi.Schema(
                title='AccessToken',
                type='object',
                properties={
                    'access': openapi.Schema(title='Access token', type='string')
                },
                required=['access']
            )
        }
    )
)
class MyRefreshView(TokenRefreshView):
    pass

api.add_router('v1/', api_routes)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api/token/refresh/', MyRefreshView.as_view(), name='token_get_access'),
    path('api/token/', MyTokenView.as_view(), name='token_obtain_pair'),
    path('api/userinfo/', UserInfoView.as_view({'get':'list'}), name='userinfo'),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/core/', include('coredb.urls')),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('api/', api.urls),
]
