from django.db import models

from .reseller import Reseller
from .parser import Parser


class Brand(models.Model):
    name = models.CharField(max_length=100)
    hb2uri = models.CharField(max_length=100, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    reseller = models.ForeignKey(Reseller, related_name='reseller_brands', on_delete=models.SET_NULL, blank=True, null=True)
    parser = models.ForeignKey(Parser, related_name='parser_brands', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return f' brand name: {self.name}, resseller {self.reseller}'

    class Meta(object):
        app_label = 'coredb'

