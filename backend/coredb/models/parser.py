from django.core.validators import MinValueValidator
from django.db import models

# from .reseller import Reseller
# from .brand import Brand




class Parser(models.Model):
    class Language(models.TextChoices):
        PHP = 'php', 'PHP'
        PY = 'py', 'PYTHON'

    name =  models.CharField(max_length=100)


    # reseller = models.ForeignKey(Reseller, related_name='reseller_parser', on_delete=models.SET_NULL, blank=True, null=True)
    # brand = models.ForeignKey(Brand, related_name='brand_parser', on_delete=models.SET_NULL, blank=True, null=True)

    code = models.TextField(blank=True, null=True)
    language = models.CharField(max_length=3, choices=Language.choices, default=Language.PHP)

    # login = models.CharField(max_length=100)
    # password = models.CharField(max_length=100)
    #
    # info = models.TextField(blank=True, null=True)
    #
    # rank = models.IntegerField(default=0, validators=[MinValueValidator(0), MinValueValidator(5)])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta(object):
        app_label = 'coredb'
