from django.db import models
from rest_framework import serializers
from .parser import Parser




class Reseller(models.Model):
    name = models.CharField(max_length=100)
    hb2uri = models.CharField(max_length=100, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    parser = models.ForeignKey(Parser, related_name='parser_reseller', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta(object):
        app_label = 'coredb'

