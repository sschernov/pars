# from rest_framework import serializers
from rest_framework_json_api import relations, serializers
from .models import *

#
class BrandSerializerSimple(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('id', 'name', 'hb2uri', 'reseller')

class ResellerSerializerSimple(serializers.ModelSerializer):
    class Meta:
        model = Reseller
        fields = ('id', 'name', 'hb2uri', 'parser')

class ResellerSerializerSimple2(serializers.ModelSerializer):
    class Meta:
        model = Reseller
        fields = ('id', 'name', 'hb2uri')

class ParserListSerializerSimple(serializers.ModelSerializer):
    class Meta:
        model = Parser
        fields =  ('id', 'name')


class BrandSerializer(serializers.ModelSerializer):
    reseller = ResellerSerializerSimple(many=False)
    parser = ParserListSerializerSimple(many=False)
    class Meta:
        model = Brand
        fields = '__all__'
        # fields = ['id', 'name', 'hb2uri', 'reseller', 'brand_parser']

class ResellerSerializer(serializers.ModelSerializer):
    reseller_brands = BrandSerializerSimple(many=True)
    # brands = BrandSerializerSimple()
    parser = ParserListSerializerSimple(many=False)
    class Meta:
        model = Reseller
        fields = '__all__'

class ParserListSerializer(serializers.ModelSerializer):
    parser_brands = BrandSerializerSimple(many=True)
    parser_reseller = ResellerSerializerSimple2(many=True)
    class Meta:
        model = Parser
        fields = '__all__'




class BrandSerializerEdit(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'


#



class ResellerSerializerEdit(serializers.ModelSerializer):
    class Meta:
        model = Reseller
        fields = '__all__'


class ParserEditSerializer(serializers.ModelSerializer):
    parser_reseller = ResellerSerializerSimple(many=True)
    parser_brands = BrandSerializerSimple(many=True)

    class Meta:
        model = Parser
        fields = '__all__'