from django.urls import path, include
from . import views
from django.urls import path, include
from rest_framework.routers import DefaultRouter, SimpleRouter


router = DefaultRouter()
# router.register(r'brands', views.BrandViewSet)
# router.register(r'resellers', views.ResellerViewSet)
# router.register(r'parsers', views.ParsersListViewSet)
router.register(r'brand',views.BrandEditViewSet)
router.register(r'reseller', views.ResellerEditViewSet)
router.register(r'parser', views.ParsersEditViewSet)

urlpatterns = [
    path('brands/',views.BrandViewSet.as_view(), name='brands'),
    path('resellers/',views.ResellerViewSet.as_view(), name='resellers'),
    path('parsers/',views.ParsersListViewSet.as_view(), name='parsers'),
    path('', include(router.urls))
]

