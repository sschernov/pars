from rest_framework import filters
from rest_framework import generics
import django_filters.rest_framework
from rest_framework.permissions import IsAuthenticated

from rest_framework_json_api.views import (
    ModelViewSet,
    ReadOnlyModelViewSet,
    RelationshipView,
)

from .models import  *

from .serializers import BrandSerializer, ResellerSerializer, BrandSerializerSimple, BrandSerializerEdit, \
    ResellerSerializerEdit, ParserListSerializer, ParserEditSerializer

from rest_framework_json_api.pagination import JsonApiPageNumberPagination

class MyPagePagination(JsonApiPageNumberPagination):
    page_query_param = 'page'
    page_size_query_param = 'limit'
    page_size = 10
    max_page_size = 1000

class CustomSearchFilter(filters.SearchFilter):
    def get_search_fields(self, view, request):
        if request.query_params.get('name'):
            return ['search']
        return super(CustomSearchFilter, self).get_search_fields(view, request)

class MyGenericView(generics.ListAPIView):
    def get_queryset(self):
        queryset = self.model.objects.all()
        name = self.request.query_params.get('name', None)
        if name:
            queryset = queryset.filter(name__contains=name)
        return queryset


class BrandViewSet(MyGenericView):
    # queryset = Brand.objects.all()
    model = Brand
    serializer_class = BrandSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = MyPagePagination

class BrandEditViewSet(ModelViewSet):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializerEdit
    permission_classes = (IsAuthenticated,)
    pagination_class = MyPagePagination


class ResellerViewSet(MyGenericView):
    # queryset = Reseller.objects.all()
    model = Reseller
    # queryset = Reseller.objects.all()
    serializer_class = ResellerSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = MyPagePagination


class ResellerEditViewSet(ModelViewSet):
    """
    API endpoint for Reseller
    """
    queryset = Reseller.objects.all()
    serializer_class = ResellerSerializerEdit
    permission_classes = (IsAuthenticated,)
    pagination_class = MyPagePagination


class ParsersListViewSet(MyGenericView):
    # queryset = Parser.objects.all()
    model = Parser
    serializer_class = ParserListSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = MyPagePagination

class ParsersEditViewSet(ModelViewSet):
    queryset = Parser.objects.all()
    serializer_class = ParserEditSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = MyPagePagination